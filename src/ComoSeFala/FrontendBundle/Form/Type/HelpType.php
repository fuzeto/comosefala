<?php

namespace ComoSeFala\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class HelpType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'value',
                'text',
                array(
                    'label'  => ' ',
                )
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'help';
    }
}
