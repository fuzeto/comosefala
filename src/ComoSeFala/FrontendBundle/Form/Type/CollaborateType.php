<?php

namespace ComoSeFala\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CollaborateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'word',
                'text',
                array(
                    'read_only' => true,
                    'label'     => 'Palavra'
                )
            )

            ->add(
                'pronounce',
                'text',
                array(
                    'label' => 'Digite abaixo a pronúncia da palavra',
                    'required'  => true,
                    'attr' => array(
                        'placeholder' => 'Ex: Apple -> pronúncia: Épol',
                    ),
                )
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'collaborate';
    }
}
