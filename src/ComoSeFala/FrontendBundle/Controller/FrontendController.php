<?php

namespace ComoSeFala\FrontendBundle\Controller;

use ComoSeFala\DomainBundle\Entity\Counter;
use ComoSeFala\DomainBundle\Entity\Word;
use ComoSeFala\DomainBundle\Service\Word\WordServiceAware;
use ComoSeFala\FrontendBundle\Form\Type\CollaborateType;
use ComoSeFala\FrontendBundle\Form\Type\HelpType;
use ComoSeFala\FrontendBundle\Form\Type\SearchFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use ComoSeFala\WebFrameworkBundle\Aware\EntityManagerAware;
use ComoSeFala\WebFrameworkBundle\Aware\FormAware;
use ComoSeFala\WebFrameworkBundle\Aware\RouterAware;
use ComoSeFala\WebFrameworkBundle\Aware\TwigAware;

class FrontendController
{
    use TwigAware;
    use EntityManagerAware;
    use FormAware;
    use RouterAware;
    use WordServiceAware;

    public function count()
    {
        $count = $this->em
            ->getRepository('DomainBundle:Counter')
            ->findOneByType(Counter::HOME);

        $count->setCount($count->getCount() + 1);

        $this->em->persist($count);
        $this->em->flush();
    }

    public function indexAction(Request $request)
    {
        $this->count();

        $value = null;

        $formFilter = $this->createForm(new SearchFilterType());
        $formFilter->handleRequest($request);

        if ($formFilter->isValid()) {
            $dataFilter = $formFilter->getData();
            $value      = $dataFilter['value'];

            $search = $this->em
                ->getRepository('DomainBundle:Word')
                ->search(
                    $value,
                    false
                );

            if (!$search) {
                return $this->render(
                    'FrontendBundle:Home:search.html.twig',
                    array(
                        'word'    => $value,
                        'exists'  => false,
                        'isHelp'  => false,
                        'results' => $search
                    )
                );
            }

            return $this->render(
                'FrontendBundle:Home:search.html.twig',
                array(
                    'word'    => $value,
                    'exists'  => true,
                    'isHelp'  => false,
                    'results' => $search
                )
            );
        }

        return $this->render(
            'FrontendBundle:Home:index.html.twig',
            array(
                'form' => $formFilter->createView()
            )
        );
    }

    public function helpAction(Request $request)
    {
        $value = null;

        $form = $this->createForm(new HelpType());
        $form->handleRequest($request);

        if ($request->get('word')) {
            $value = $request->get('word');

            $search = $this->em
                ->getRepository('DomainBundle:Word')
                ->search(
                    $value,
                    true
                );

            if ($search) {
                return $this->render(
                    'FrontendBundle:Home:search.html.twig',
                    array(
                        'word'    => $value,
                        'exists'  => true,
                        'isHelp'  => true,
                        'results' => $search
                    )
                );
            }

            $word = new Word();

            $this->wordService->create($word, $value);

            return $this->redirect(
                $this->generateUrl(
                    'frontend_help',
                    [
                        'created' => true
                    ]
                )
            );
        }

        if ($form->isValid()) {
            $dataFilter = $form->getData();
            $value      = $dataFilter['value'];

            $search = $this->em
                ->getRepository('DomainBundle:Word')
                ->search(
                    $value,
                    true
                );

            if ($search) {
                return $this->render(
                    'FrontendBundle:Home:search.html.twig',
                    array(
                        'word'    => $value,
                        'exists'  => true,
                        'results' => $search
                    )
                );
            }

            $word = new Word();

            $this->wordService->create($word, $value);

            return $this->redirect(
                $this->generateUrl(
                    'frontend_help',
                    [
                        'created' => true
                    ]
                )
            );

        }

        return $this->render(
            'FrontendBundle:Home:help.html.twig',
            array(
                'created' => $request->get('created'),
                'form'    => $form->createView()
            )
        );
    }

    public function collaborateAction()
    {
        $search = $this->em
            ->getRepository('DomainBundle:Word')
            ->findBy(
                [
                    'pronounce' => null
                ]
            );

        return $this->render(
            'FrontendBundle:Home:collaborate.html.twig',
            array(
                'results' => $search
            )
        );
    }

    public function collaborateUpdateAction(Request $request, Word $word)
    {
        $form   = $this->form->create(new CollaborateType(), $word);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->wordService->update($word);
            return $this->render('FrontendBundle:Home:thanksForCollaborate.html.twig');
        }

        return $this->render(
            'FrontendBundle:Home:collaborateAction.html.twig',
            array(
                'entity' => $word,
                'form'   => $form->createView(),
            )
        );

    }

}
