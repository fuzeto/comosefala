<?php

namespace Wideti\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Wideti\DomainBundle\Validator\Constraints as MyAssert;

/**
 * @ORM\Table(name="job_applicant")
 * @ORM\Entity(repositoryClass="Wideti\DomainBundle\Repository\JobApplicantRepository")
 */
class JobApplicant
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Course", mappedBy="jobApplicant", fetch="EAGER")
     */
    private $courses;

    /**
     * @ORM\OneToMany(targetEntity="Education", mappedBy="jobApplicant", cascade={"persist"}, fetch="EAGER")
     */
    private $educations;

    /**
     * @ORM\OneToMany(targetEntity="Experience", mappedBy="jobApplicant", cascade={"persist"}, fetch="EAGER")
     */
    private $experiences;

    /**
     * @ORM\OneToMany(targetEntity="Language", mappedBy="jobApplicant", cascade={"persist"}, fetch="EAGER")
     */
    private $languages;

    /**
     * @ORM\ManyToMany(targetEntity="Wideti\DomainBundle\Entity\Position", mappedBy="jobApplicant")
     */
    protected $positions;

    /**
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank(message="Digite o nome")
     */
    private $name;

    /**
     * @ORM\Column(name="cpf_number", type="string", length=100)
     * @Assert\NotBlank(message="Digite o CPF")
     * @MyAssert\CpfIsValid()
     */
    private $cpfNumber;

    /**
     * @ORM\Column(name="rg_number", type="string", length=100)
     * @Assert\NotBlank(message="Digite o RG")
     */
    private $rgNumber;

    /**
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     * @Assert\NotBlank(message="Digite a data de nascimento")
     * @Assert\Date(
     *      message = "Data invalida."
     *  )
     */
    private $birthDate;

    /**
     * @ORM\Column(name="gender", type="string", nullable=true)
     * @Assert\NotBlank(message="Selecione o sexo do candidato")
     * @Assert\Choice(
     *      choices = {"Feminino","Masculino"},
     *      message = "Sexo invalido."
     *  )
     */
    private $gender;

    /**
     * @ORM\Column(name="drive_license", type="string", nullable=true)
     * @Assert\NotBlank(message="Selecione a categoria da CNH")
     * @Assert\Choice(
     *      choices = {"A","B","C","D","E"},
     *      message = "Categoria Inválida."
     *  )
     */
    private $driveLicense;

    /**
     * @ORM\Column(name="email1", type="string", length=100, nullable=true)
     * @Assert\Email(
     *  message   = "Email inválido ou não existente",
     *  checkMX   = true,
     *  checkHost = true
     * )
     */
    private $email1;

    /**
     * @ORM\Column(name="email2", type="string", length=100, nullable=true)
     * @Assert\Email(
     *  message   = "Email inválido ou não existente",
     *  checkMX   = true,
     *  checkHost = true
     * )
     */
    private $email2;

    /**
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     * @Assert\NotBlank(message="Digite o telefone")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(name="additional_phone_number", type="string", length=20, nullable=true)
     */
    private $additionalPhoneNumber;

    /**
     * @ORM\Column(name="additional_phone_number_2", type="string", length=20, nullable=true)
     */
    private $additionalPhoneNumber2;

    /**
     * @ORM\Column(name="occupation", type="string", length=20, nullable=true)
     */
    private $occupation;

    /**
     * @ORM\Column(name="zip_code", type="string", length=15)
     * @Assert\NotBlank(message="Digite o CEP")
     */
    private $zipCode;

    /**
     * @ORM\Column(name="address", type="string", length=100)
     * @Assert\NotBlank(message="Digite o endereço")
     */
    private $address;

    /**
     * @ORM\Column(name="state", type="string", length=30)
     * @Assert\NotBlank(message="Selecione o estado")
     */
    private $state;

    /**
     * @ORM\Column(name="city", type="string", length=100)
     * @Assert\NotBlank(message="Digite a cidade")
     */
    private $city;

    /**
     * @ORM\Column(name="additional_address_info", type="string", length=255, nullable=true)
     */
    private $additionalAddressInfo;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime",nullable=false)
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->courses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->educations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->experiences = new \Doctrine\Common\Collections\ArrayCollection();
        $this->languages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->positions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return JobApplicant
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cpfNumber
     *
     * @param string $cpfNumber
     * @return JobApplicant
     */
    public function setCpfNumber($cpfNumber)
    {
        $this->cpfNumber = $cpfNumber;

        return $this;
    }

    /**
     * Get cpfNumber
     *
     * @return string 
     */
    public function getCpfNumber()
    {
        return $this->cpfNumber;
    }

    /**
     * Set rgNumber
     *
     * @param string $rgNumber
     * @return JobApplicant
     */
    public function setRgNumber($rgNumber)
    {
        $this->rgNumber = $rgNumber;

        return $this;
    }

    /**
     * Get rgNumber
     *
     * @return string 
     */
    public function getRgNumber()
    {
        return $this->rgNumber;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return JobApplicant
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime 
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return JobApplicant
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set driveLicense
     *
     * @param string $driveLicense
     * @return JobApplicant
     */
    public function setDriveLicense($driveLicense)
    {
        $this->driveLicense = $driveLicense;

        return $this;
    }

    /**
     * Get driveLicense
     *
     * @return string 
     */
    public function getDriveLicense()
    {
        return $this->driveLicense;
    }

    /**
     * Set email1
     *
     * @param string $email1
     * @return JobApplicant
     */
    public function setEmail1($email1)
    {
        $this->email1 = $email1;

        return $this;
    }

    /**
     * Get email1
     *
     * @return string 
     */
    public function getEmail1()
    {
        return $this->email1;
    }

    /**
     * Set email2
     *
     * @param string $email2
     * @return JobApplicant
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2
     *
     * @return string 
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return JobApplicant
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set additionalPhoneNumber
     *
     * @param string $additionalPhoneNumber
     * @return JobApplicant
     */
    public function setAdditionalPhoneNumber($additionalPhoneNumber)
    {
        $this->additionalPhoneNumber = $additionalPhoneNumber;

        return $this;
    }

    /**
     * Get additionalPhoneNumber
     *
     * @return string 
     */
    public function getAdditionalPhoneNumber()
    {
        return $this->additionalPhoneNumber;
    }

    /**
     * Set additionalPhoneNumber2
     *
     * @param string $additionalPhoneNumber2
     * @return JobApplicant
     */
    public function setAdditionalPhoneNumber2($additionalPhoneNumber2)
    {
        $this->additionalPhoneNumber2 = $additionalPhoneNumber2;

        return $this;
    }

    /**
     * Get additionalPhoneNumber2
     *
     * @return string 
     */
    public function getAdditionalPhoneNumber2()
    {
        return $this->additionalPhoneNumber2;
    }

    /**
     * Set occupation
     *
     * @param string $occupation
     * @return JobApplicant
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get occupation
     *
     * @return string 
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return JobApplicant
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return JobApplicant
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return JobApplicant
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return JobApplicant
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set additionalAddressInfo
     *
     * @param string $additionalAddressInfo
     * @return JobApplicant
     */
    public function setAdditionalAddressInfo($additionalAddressInfo)
    {
        $this->additionalAddressInfo = $additionalAddressInfo;

        return $this;
    }

    /**
     * Get additionalAddressInfo
     *
     * @return string 
     */
    public function getAdditionalAddressInfo()
    {
        return $this->additionalAddressInfo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return JobApplicant
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return JobApplicant
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add courses
     *
     * @param \Wideti\DomainBundle\Entity\Course $courses
     * @return JobApplicant
     */
    public function addCourse(\Wideti\DomainBundle\Entity\Course $courses)
    {
        $this->courses[] = $courses;

        return $this;
    }

    /**
     * Remove courses
     *
     * @param \Wideti\DomainBundle\Entity\Course $courses
     */
    public function removeCourse(\Wideti\DomainBundle\Entity\Course $courses)
    {
        $this->courses->removeElement($courses);
    }

    /**
     * Get courses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * Add educations
     *
     * @param \Wideti\DomainBundle\Entity\Education $educations
     * @return JobApplicant
     */
    public function addEducation(\Wideti\DomainBundle\Entity\Education $educations)
    {
        $this->educations[] = $educations;

        return $this;
    }

    /**
     * Remove educations
     *
     * @param \Wideti\DomainBundle\Entity\Education $educations
     */
    public function removeEducation(\Wideti\DomainBundle\Entity\Education $educations)
    {
        $this->educations->removeElement($educations);
    }

    /**
     * Get educations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEducations()
    {
        return $this->educations;
    }

    /**
     * Add experiences
     *
     * @param \Wideti\DomainBundle\Entity\Experience $experiences
     * @return JobApplicant
     */
    public function addExperience(\Wideti\DomainBundle\Entity\Experience $experiences)
    {
        $this->experiences[] = $experiences;

        return $this;
    }

    /**
     * Remove experiences
     *
     * @param \Wideti\DomainBundle\Entity\Experience $experiences
     */
    public function removeExperience(\Wideti\DomainBundle\Entity\Experience $experiences)
    {
        $this->experiences->removeElement($experiences);
    }

    /**
     * Get experiences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExperiences()
    {
        return $this->experiences;
    }

    /**
     * Add languages
     *
     * @param \Wideti\DomainBundle\Entity\Language $languages
     * @return JobApplicant
     */
    public function addLanguage(\Wideti\DomainBundle\Entity\Language $languages)
    {
        $this->languages[] = $languages;

        return $this;
    }

    /**
     * Remove languages
     *
     * @param \Wideti\DomainBundle\Entity\Language $languages
     */
    public function removeLanguage(\Wideti\DomainBundle\Entity\Language $languages)
    {
        $this->languages->removeElement($languages);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->getName();
    }


}
