<?php

namespace ComoSeFala\DomainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserRepository extends EntityRepository implements UserProviderInterface
{
    public function count()
    {
        $q = $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->where('u.email <> :user')
            ->setParameter('user', 'admin@admin.com')
            ->getQuery();

        return $q->getSingleScalarResult();
    }

    public function allExceptMe($user)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->select('u')
            ->where('u.id != :user_id')
            ->setParameter('user_id', $user->getId())
            ->orderBy('u.name', 'ASC')
        ;
        return $q->getQuery()->getResult();
    }

    public function loadUserByUsername($username)
    {
        try {
            $user = $this->loadUserByUsernameOrEmail($username);
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active User identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    public function loadUserByUsernameOrEmail($username)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->select('u, r')
            ->leftJoin('u.roles', 'r')
            ->where('u.email = :email')
            ->setParameter('email', $username)
            ->getQuery();

        return $q->getSingleResult();
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);

        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
        || is_subclass_of($class, $this->getEntityName());
    }
}