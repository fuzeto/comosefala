<?php

namespace ComoSeFala\DomainBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class WordRepository extends EntityRepository
{
    public function count()
    {
        $q = $this->createQueryBuilder('w')
            ->select('COUNT(w.id)')
            ->getQuery();

        return $q->getSingleScalarResult();
    }

    public function all()
    {
        $qb = $this->createQueryBuilder('w')
            ->select('w')
            ->orderBy('w.id', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }

    public function search($value = null, $createAction)
    {
        $qb = $this->createQueryBuilder('w')
            ->where('w.word LIKE :value')
            ->orderBy('w.word', 'ASC')
            ->setParameter('value', "%$value%");

        if ($createAction === false) {
            $qb->andWhere('w.pronounce IS NOT NULL');
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }
}