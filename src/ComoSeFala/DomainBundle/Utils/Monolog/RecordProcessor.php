<?php
/**
 * Created by ComoSeFala.
 * User: sergiocosta, leonardofuzeto
 * Date: 9/25/14, 9/29/14
 * Time: 12:48 PM, 09:25 AM
 */

namespace ComoSeFala\DomainBundle\Utils\Monolog;

use Symfony\Component\DependencyInjection\ContainerInterface;
use ComoSeFala\DomainBundle\Entity\SysUser;

class RecordProcessor
{
    protected $container;

    private $record;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param  array $record
     * @return array
     */
    public function processEmailRecord(array $record)
    {
        $this->record = $record;

        if ($_SERVER) {
            $this->record['extra']['SERVER']   = $_SERVER;
        }

        if ($_REQUEST) {
            $this->record['extra']['REQUEST']  = $_REQUEST;
        }

        if ($_POST) {
            $this->record['extra']['POST']     = $_POST;
        }

        if ($_FILES) {
            $this->record['extra']['FILES']    = $_FILES;
        }

        $this->getUserInfo();

        return $this->record;
    }

    /**
     * @param  array $record
     * @return array
     */
    public function processLogRecord(array $record)
    {
        $this->record = $record;

        $this->getUserInfo();

        return $this->record;
    }

    /**
     * @return $this
     */
    private function getUserInfo()
    {
        $securityContext = $this->container->get('security.context');

        if ($securityContext->getToken() == null) {
            return true;
        }

        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->record['extra']['User'] = 'User was not authenticated';

            return true;
        }

        $user = $securityContext->getToken()->getUser();

        if ($user instanceof SysUser) {
            $this->record['extra']['Clients']['id']     = $user->getId();
            $this->record['extra']['Clients']['name']   = $user->getName();
            $this->record['extra']['Clients']['email']  = $user->getEmail();
        } elseif ($user instanceof \ComoSeFala\DomainBundle\Entity\ClientUsers) {
            $this->record['extra']['User']['id']        = $user->getId();
            $this->record['extra']['User']['name']      = $user->getName();
            $this->record['extra']['User']['username']  = $user->getEmail();
        } else {
            $this->record['extra']['user']['user'] = "User is not identified, CHECK THIS OUT!.";
        }

        return true;
    }
}
