<?php

namespace ComoSeFala\DomainBundle\Utils;

use Guzzle\Http\EntityBody;

class FileHandler extends \SplFileInfo
{
    /**
     * Services handlers
     *
     * @ServiceContainer
     * @FileUploader
     */
    private $container;
    private $file_uploader;
    /**
     * configuration parameters
     */
    private $profile_folder;
    private $signature_folder;
    private $logo_folder;
    private $mi_folder;
    private $mi_comments_folder;
    private $mi_anamnesis;
    /**
     * set url in doom mode
     */
    private $disaster_url;

    /**
     * Receive and configure service and parameters
     *
     * @param $container
     * @param $disaster_url
     */
    public function __construct($container, $disaster_url)
    {
        /**
         * start services
         */
        $this->container      = $container;
        $this->file_uploader  = $container->get('file_uploader');
        /**
         * configure folders
         */
        $this->profile_folder       = $container->getParameter('aws_profile_pictures_folder');
        $this->signature_folder     = $container->getParameter('aws_signatures_folder');
        $this->logo_folder          = $container->getParameter('aws_clients_logo_folder');
        $this->mi_folder            = $container->getParameter('aws_medical_interaction_folder');
        $this->mi_comments_folder   = $container->getParameter('aws_comment_files_folder');
        $this->mi_anamnesis         = $container->getParameter('aws_anamnesis_folder');

        /**
         * doom folder
         */
        $this->disaster_url   = $disaster_url;
    }

    /**
     * Check if system is in disaster_recovery
     *
     * @return boolean
     */
    public function isDisaster()
    {
        return $this->container->getParameter('disaster_recovery');
    }

    /**
     * return which method use to retrieve file info
     *
     * @param $type
     * @return string
     */
    public function getMethod($type = 'url')
    {
        /**
         * Choose correct method to return a string with url
         * or an file object
         */
        switch ($type) {
            case 'url':
                $method = 'getUrl';
                break;
            case 'object':
                $method = 'getDownloadUrl';
                break;
            default:
                $method = 'getUrl';
                break;
        }

        return $method;
    }

    /**
     * retrieve profile picture url from user
     *
     * @param  string $file file name
     * @return string file url
     */
    public function profile($file)
    {
        if (!$this->isDisaster()) {
            return $this->file_uploader->getUrl(
                $file,
                $this->profile_folder,
                30
            );
        }

        return $this->disaster_url.$this->profile_folder.'/'.$file;
    }

    /**
     * retrieve signature url from user
     *
     * @param  string $file file name
     * @return string file url
     */
    public function signature($file)
    {
        if (!$this->isDisaster()) {
            $url = $this->file_uploader->getUrl(
                $file,
                $this->signature_folder,
                30
            );

            return ($url);
        }

        return $this->disaster_url.$this->signature_folder.'/'.$file;
    }

    /**
     * retrieve signature url from user
     *
     * @param  string $file file name
     * @return string file url
     */
    public function logo($file)
    {
        if (!$this->isDisaster()) {
            $url = $this->file_uploader->getUrl(
                $file,
                $this->logo_folder,
                30
            );

            return ($url);
        }

        return $this->disaster_url.$this->logo_folder.'/'.$file;
    }

    /**
     * retrieve medical interaction file
     *
     * @param string $file name
     * @param string $type url/object
     *
     * @return mixed
     */
    public function medicalInteraction($file, $type = 'url')
    {
        $method = $this->getMethod($type);
        /**
         * Check if disaster_recovery is enabled
         */
        if (!$this->isDisaster()) {
            /**
             * retrieve file from AWS S3
             */
            return $this->file_uploader->$method(
                $file,
                $this->mi_folder,
                1
            );
        }
        /**
         * set local url to file
         */
        $pathToResource = $this->disaster_url.$this->mi_folder.'/'.$file;
        /**
         * return just a url
         */
        if ($type == 'url') {
            return $pathToResource;
        }
        /**
         * return a converted file objected based on URL
         */
        return $this->pathToObject($pathToResource);
    }

    /**
     * retrieve medical interaction file
     *
     * @param string $file name
     * @param string $type url/object
     *
     * @return mixed
     */
    public function anamnesis($file, $type = 'url')
    {
        $method = $this->getMethod($type);
        /**
         * Check if disaster_recovery is enabled
         */
        if (!$this->isDisaster()) {
            /**
             * retrieve file from AWS S3
             */
            return $this->file_uploader->$method(
                $file,
                $this->mi_anamnesis,
                1
            );
        }
        /**
         * set local url to file
         */
        $pathToResource = $this->disaster_url.$this->mi_anamnesis.'/'.$file;
        /**
         * return just a url
         */
        if ($type == 'url') {
            return $pathToResource;
        }
        /**
         * return a converted file objected based on URL
         */
        return $this->pathToObject($pathToResource);
    }

    /**
     * retrieve medical interaction comment file
     *
     * @param string $file name
     * @param string $type url/object
     *
     * @return string file url
     */
    public function medicalInteractionComment($file, $type = 'url')
    {
        $method = $this->getMethod($type);
        /**
         * Check if disaster_recovery is enabled
         */
        if (!$this->isDisaster()) {
            /**
             * retrieve file from AWS S3
             */
            return $this->file_uploader->$method(
                $file,
                $this->mi_comments_folder,
                1
            );
        }
        /**
         * set local url to file
         */
        $pathToResource = $this->disaster_url.$this->mi_comments_folder.'/'.$file;
        /**
         * return just a url
         */
        if ($type == 'url') {
            return $pathToResource;
        }
        /**
         * return a converted file objected based on URL
         */
        return $this->pathToObject($pathToResource);
    }

    /**
     * Translate a url path in a file object similar to an S3 object
     *
     * @param $absolutePath string "ex.: http://localhost/web/uploads/file.jpg"
     * @return array "with file object"
     */
    public function pathToObject($absolutePath)
    {
        $response = array();
        $body     = new EntityBody(fopen($absolutePath, 'r'));

        $response['Body']        = $body;
        $response['ContentType'] = $body->getContentType();

        return $response;
    }
}
