<?php

namespace ComoSeFala\DomainBundle\Utils;

class GeneratePdf extends \mPDF
{
    public function __construct()
    {
        parent::mPDF();

        $this->debug                    = true;
        $this->showImageErrors          = true;
        $this->allow_charset_conversion = false;
    }

    public function Error($msg)
    {
        throw new \Exception($msg);
    }
}
