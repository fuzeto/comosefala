<?php

namespace ComoSeFala\DomainBundle\Utils;

use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\File\File;
use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUpload extends \SplFileInfo
{
    /**
     * @var S3Client
     */
    protected $s3;

    /**
     * @var string api key from AWS
     * @var string secret key from AWS
     */
    protected $aws_key;
    protected $aws_secret;

    /**
     * @var string name of bucket
     */
    protected $bucket;

    /**
     * @ServiceContainer
     */
    private $container;
    public $disaster_recovery;

    public function __construct($aws_key, $aws_secret, $bucket, $container)
    {
        $this->aws_key              = $aws_key;
        $this->aws_secret           = $aws_secret;
        $this->bucket               = $bucket;
        $this->container            = $container;
        $this->disaster_recovery    = $container->getParameter('disaster_recovery');

        $this->s3 = S3Client::factory(array(
            'key'       => $this->aws_key,
            'secret'    => $this->aws_secret,
        ));
    }

    /**
     * check if system is in disaster_recovery
     *
     * @return bool
     */
    public function isDisaster()
    {
        return $this->disaster_recovery;
    }

    /**
     * create a unique name for file
     *
     * @param $file UploadedFile
     *
     * @return string
     */
    public function generateFileName($file)
    {
        return uniqid(time()."_", true).'.'.$file->guessExtension();
    }

    /**
     * Upload a file to AWS S3 or local folder
     *
     * @param object|\Symfony\Component\HttpFoundation\File\UploadedFile $file object UploadedFile
     * @param string|filename
     * @param $folder string
     *
     * @return boolean
     */
    public function uploadFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file, $fileName, $folder)
    {
        if (!$this->isDisaster()) {
            return $this->uploadFileToS3($file->getRealPath(), $fileName, $folder, $file);
        }

        return $this->uploadLocalFile($file, $fileName, $folder);
    }

    /**
     * Upload a file to local folder
     *
     * @param UploadedFile $file
     * @param string       $fileName
     * @param string       $folder
     *
     * @return bool
     */
    public function uploadLocalFile(UploadedFile $file, $fileName, $folder)
    {
        $uploadDir = __DIR__."/../../../../web/uploads/".$folder;

        $file->move(
            $uploadDir,
            $fileName
        );

        return true;
    }

    /**
     * @param $originPath string original location of file
     * @param $fileName string final name of file
     * @param $folder string name of folder that the file goes
     * @param object|\Symfony\Component\HttpFoundation\File\UploadedFile $file object UploadedFile
     *
     * @return \Guzzle\Service\Resource\Model
     */
    private function uploadFileToS3($originPath, $fileName, $folder, \Symfony\Component\HttpFoundation\File\UploadedFile $file)
    {
        return $this->s3->putObject(array(
            'ACL'            => 'public-read',
            'Bucket'         => $this->bucket,
            'Key'            => $folder.'/'.$fileName,
            'SourceFile'     => $originPath,
            'ContentType'    => $file->getClientMimeType(),
        ));
    }

    /**
     * @param $fileName string
     * @param $folder string
     *
     * @return mixed
     */
    public function deleteFile($fileName, $folder)
    {
        if (!$this->isDisaster()) {
            return $this->s3->deleteObject(array(
                'Bucket' => $this->bucket,
                'Key'    => $folder.'/'.$fileName,
            ));
        }

        return $this->deleteLocalFile($fileName, $folder);
    }

    /**
     * delete local file
     *
     * @param $filename string
     * @param $folder string
     *
     * @return bool
     */
    public function deleteLocalFile($filename, $folder)
    {
        $fileDir = __DIR__."/../../../../web/uploads/".$folder;

        try {
            unlink($fileDir.'/'.$filename);
        } catch (ContextErrorException $e) {
            $logger = $this->container->get('logger');
            $logger->error("Erro ao remover arquivo: ".$fileDir.'/'.$filename);
        }

        return true;
    }

    /**
     * check if file exists on S3
     *
     * @param $folder string
     * @param $fileName string
     *
     * @return boolean
     */
    public function existsFile($folder, $fileName)
    {
        return $this->s3->doesObjectExist(
            $this->bucket,
            $folder.'/'.$fileName
        );
    }

    /**
     * download a file from S3
     *
     * @param $fileName
     * @param $folder
     *
     * @return \Guzzle\Service\Resource\Model
     */
    public function getDownloadUrl($fileName, $folder)
    {
        if ($this->existsFile($folder, $fileName)) {
            $object = $this->s3->getObject(array(
                'Bucket' => $this->bucket,
                'Key' => $folder.'/'.$fileName,
            ));

            return $object;
        }
    }

    /**
     * get a file url from S3
     *
     * @param $fileName string
     * @param $folder string
     * @param $time
     *
     * @return string
     */
    public function getUrl($fileName, $folder, $time)
    {
        if ($this->existsFile($folder, $fileName)) {
            $url = $this->s3->getObjectUrl(
                $this->bucket,
                $folder.'/'.$fileName,
                "+{$time} minutes"
            );

            return $url;
        }
    }

    /**
     * a breakpoint only used for unit tests
     *
     * @param  boolean $file
     * @return bool
     */
    public function testMethod($file = true)
    {
        return $file;
    }
}
