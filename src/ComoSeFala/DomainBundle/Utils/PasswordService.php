<?php

namespace ComoSeFala\DomainBundle\Utils;

use Doctrine\ORM\EntityManager;
use Twig_Environment;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Util\SecureRandom;

/**
 * Simple Password service, provide functionality for the most common
 * uses for password.
 *
 * Create New Password
 * Reset Password
 * Re-generate Password
 * Forgot my password
 *
 */
class PasswordService extends \SplFileInfo
{
    /**
     * @var EncoderFactory
     */
    protected $factory;

    /**
     * @var object Mailer
     */
    protected $mailer;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * Dependency Injection
     *
     * @param EncoderFactory   $encoder
     * @param object           $mailer
     * @param EntityManager    $em
     * @param Twig_Environment $twig
     */
    public function __construct(EncoderFactory $encoder, $mailer, EntityManager $em, Twig_Environment $twig)
    {
        $this->factory = $encoder;
        $this->mailer  = $mailer;
        $this->em      = $em;
        $this->twig    = $twig;
    }

    /**
     * Generate a new random password for Generic User entity
     *
     * @param UserInterface $entity
     *
     * @return array
     */
    public function generate(UserInterface $entity)
    {
        $generator       = new SecureRandom();
        $encoder         = $this->factory->getEncoder($entity);
        $plainPassword   = bin2hex($generator->nextBytes(4));

        $encodedPassword = $encoder->encodePassword(
            $plainPassword,
            $entity->getSalt()
        );

        return array(
            'plain'     => $plainPassword,
            'encoded'   => $encodedPassword,
        );
    }

    /**
     * Encode a given password string
     *
     * @param UserInterface $entity
     * @param string        $password
     *
     * @return string encoded password
     */
    public function encodePassword(UserInterface $entity, $password)
    {
        $encoder     = $this->factory->getEncoder($entity);

        $newPassword = $encoder->encodePassword(
            $password,
            $entity->getSalt()
        );

        return $newPassword;
    }

    /**
     * Handles with generate new password and re-send it to
     * the user
     *
     * @param UserInterface $entity
     *
     * @return mixed
     */
    public function forgot(UserInterface $entity)
    {
        /**
         * create new password
         */
        $newPassword = $this->generate($entity);
        /**
         * then update the entity
         */
        $entity->setPassword($newPassword['encoded']);
        $this->em->persist($entity);
        $this->em->flush();
        /**
         * send email with password
         */
        return $this->sendResetMail($entity, $newPassword['plain']);
    }

    /**
     * Send e-mail with new user password
     *
     * @param UserInterface $entity
     * @param string        $password
     *
     * @return mixed
     */
    public function sendResetMail(UserInterface $entity, $password)
    {
        /**
         * create the message
         */
        $message = \Swift_Message::newInstance()
            ->setSubject('Esqueci Minha Senha')
            ->setFrom(array(
                'no-reply@comosefala.com' => 'No-reply Como se fala?',
            ))
            ->setTo($entity->getEmail())
            ->setContentType('text/html')
            ->setBody(
                $this->twig->render(
                    'AdminBundle:SysUser:reset_password.html.twig',
                    array(
                        'password'  => $password,
                        'entity'    => $entity,
                    )
                )
            )
        ;
        /**
         * send message
         */
        $this->mailer->send($message);
        /**
         * return an object with message
         */
        return $this->getMessage('success', 'Uma nova senha foi enviada para o seu e-mail');
    }

    /**
     * Create an message object to return an error/success/notification
     *
     * @param string $type
     * @param string $message
     *
     * @return mixed
     */
    public function getMessage($type, $message)
    {
        $object             = new \StdClass();
        $object->type       = $type;
        $object->text       = $message;

        return $object;
    }
}
