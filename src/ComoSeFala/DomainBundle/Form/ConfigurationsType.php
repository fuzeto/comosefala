<?php

namespace ComoSeFala\DomainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ConfigurationsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'about',
                'textarea',
                array(
                    'label' => 'Sobre Nós',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            );

        $builder->add('submit', 'submit', array('label' => 'Salvar'));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ComoSeFala\DomainBundle\Entity\Configurations'
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'configurations';
    }
}
