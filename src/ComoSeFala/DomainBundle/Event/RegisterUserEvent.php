<?php

namespace ComoSeFala\DomainBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use ComoSeFala\DomainBundle\Entity\User;

class RegisterUserEvent extends Event
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}