<?php

namespace ComoSeFala\DomainBundle\Service\Word;

use Doctrine\ORM\NoResultException;
use Symfony\Component\Config\Definition\Exception\Exception;
use ComoSeFala\DomainBundle\Entity\Word;
use Symfony\Component\EventDispatcher\EventDispatcher;

use ComoSeFala\WebFrameworkBundle\Aware\EntityManagerAware;
use ComoSeFala\WebFrameworkBundle\Aware\FlashMessageAware;
use Doctrine\DBAL\DBALException;
use ComoSeFala\WebFrameworkBundle\Aware\TwigAware;

class WordService
{
    use EntityManagerAware;
    use FlashMessageAware;
    use TwigAware;

    /**
     * @param Word $word
     */
    public function register(Word $word)
    {
        $this->em->persist($word);
        $this->em->flush();

        $this->setFlashMessage('notification', 'Palavra cadastrada com sucesso');
    }

    /**
     * @param Word $word
     */
    public function update(Word $word)
    {
        $this->em->persist($word);
        $this->em->flush();

        $this->setFlashMessage('notification', 'Palavra alterada com sucesso');
    }

    /**
     * @param Word $word
     * @return bool
     */
    public function delete(Word $word)
    {
        try{
            $this->em->remove($word);
            $this->em->flush();
        } catch(DBALException $e) {
            $this->setFlashMessage('error', 'Você não pode remover esta Palavra');
            return false;
        }
        $this->setFlashMessage('notification', 'Palavra removida com sucesso');
    }

    public function create(Word $word, $value)
    {
        $word->setWord($value);

        $this->em->persist($word);
        $this->em->flush();
    }
}