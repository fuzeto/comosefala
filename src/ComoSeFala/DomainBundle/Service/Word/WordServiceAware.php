<?php

namespace ComoSeFala\DomainBundle\Service\Word;

use ComoSeFala\DomainBundle\Service\Word\WordService;

/**
 *
 * Usage: - [ setWordService, [@core.service.word_service] ]
 */
trait WordServiceAware
{
    /**
     * @var WordService
     */
    protected $wordService;

    public function setWordService(WordService $service)
    {
        $this->wordService = $service;
    }
}
