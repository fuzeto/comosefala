<?php

namespace ComoSeFala\DomainBundle\Service\User;

use Doctrine\ORM\NoResultException;
use Symfony\Component\Config\Definition\Exception\Exception;
use ComoSeFala\DomainBundle\Event\RegisterUserEvent;
use ComoSeFala\DomainBundle\Entity\User;
use ComoSeFala\DomainBundle\Service\Password\PasswordService;
use Symfony\Component\EventDispatcher\EventDispatcher;

use ComoSeFala\WebFrameworkBundle\Aware\EntityManagerAware;
use ComoSeFala\WebFrameworkBundle\Aware\EventDispatcherAware;
use ComoSeFala\WebFrameworkBundle\Aware\FlashMessageAware;
use Doctrine\DBAL\DBALException;
use ComoSeFala\WebFrameworkBundle\Aware\MailerAware;
use ComoSeFala\WebFrameworkBundle\Aware\TwigAware;

class UserService
{
    use EntityManagerAware;
    use EventDispatcherAware;
    use TwigAware;
    use MailerAware;

    /**
     * @var PasswordService
     */
    protected $passwordService;

    /**
     * @param User       $user
     */
    public function register(User $user)
    {
        $password = $this->passwordService->generate($user);
        $user->setPassword($password['encoded']);

        $this->em->persist($user);
        $this->em->flush();

        $this->sendWelcomeMail($user, $password['plain']);
    }

    /**
     * @param User $user
     */
    public function update(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function delete(User $user)
    {
        try {
            $this->em->remove($user);
            $this->em->flush();
        } catch (DBALException $e) {
            throw new \Exception;
        }
    }

    /**
     * @param User $user
     */
    public function resetPassword(User $user)
    {
        $this->passwordService->forgot($user);
    }

    public function forgotPassword($emailOrUsername)
    {
        try {
            $this->resetPassword(
                $this->em
                    ->getRepository('DomainBundle:User')
                    ->loadUserByUsernameOrEmail($emailOrUsername)
            );
        } catch ( NoResultException $e ) {

            throw new Exception;

        }
    }

    public function changePassword(User $user, $password)
    {
        $newPassword = $this->passwordService->encodePassword($user, $password);

        $user->setPassword($newPassword);

        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * @param EventDispatcher $eventDispatcher
     */
    public function setEventDispatcher($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param PasswordService $ps
     */
    public function setPasswordService(PasswordService $ps)
    {
        $this->passwordService = $ps;
    }

    private function sendWelcomeMail(User $user, $password)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Bem vindo ao Como se fala?')
            ->setFrom(array('no-reply@comosefala.com' => 'No-reply Como se fala?'))
            ->setTo($user->getEmail())
            ->setContentType('text/html')
            ->setBody(
                $this->renderView(
                    'AdminBundle:User:emailBemVindo.html.twig',
                    array(
                        'password' => $password,
                        'user'     => $user->getName(),
                        'email'    => $user->getEmail(),
                    )
                )
            );

        $this->mailer->send($message);
    }
}