<?php

namespace ComoSeFala\DomainBundle\Service\User;

use ComoSeFala\DomainBundle\Service\User\UserService;

trait UserServiceAware
{
    /**
     * @var UserService
     */
    protected $userService;

    public function setUserService(UserService $service)
    {
        $this->userService = $service;
    }
}
