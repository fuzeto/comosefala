<?php

namespace ComoSeFala\WebFrameworkBundle\Aware;

use Doctrine\ORM\EntityManager;

trait EntityManagerAware
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
}