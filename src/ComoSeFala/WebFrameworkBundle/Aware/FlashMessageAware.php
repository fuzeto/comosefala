<?php

namespace ComoSeFala\WebFrameworkBundle\Aware;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Symfony Server Setup: - [ setSession, [@session] ]
 */
trait FlashMessageAware
{
    use SessionAware;

    public function setFlashMessage($type, $message)
    {
        $this->session->getFlashBag()->add(
            $type,
            $message
        );
    }

}