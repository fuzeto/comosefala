<?php

namespace ComoSeFala\WebFrameworkBundle\Aware;

/**
 * Symfony Server Setup: - [ setMailer, [@mailer] ]
 */
trait MailerAware
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    public function setMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

}