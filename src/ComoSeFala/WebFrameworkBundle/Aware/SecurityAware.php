<?php

namespace ComoSeFala\WebFrameworkBundle\Aware;

use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Symfony Server Setup: - [ setSecurityContext, [@security.context] ]
 */
trait SecurityAware
{
    /**
     * @var SecurityContextInterface
     */
    protected $securityContext;

    public function getUser()
    {
        if (null === $token = $this->securityContext->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            return;
        }
        return $user;
    }

    public function setSecurityContext(SecurityContextInterface $securityContext)
    {
        $this->securityContext = $securityContext;
    }

}