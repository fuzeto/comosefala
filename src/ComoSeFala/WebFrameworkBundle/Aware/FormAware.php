<?php

namespace ComoSeFala\WebFrameworkBundle\Aware;

use Symfony\Component\Form\FormFactory;

/**
 * Symfony Server Setup: - [ setForm, [@form.factory] ]
 */
trait FormAware
{
    /**
     * @var FormFactory
     */
    protected $form;

    public function setForm(FormFactory $form)
    {
        $this->form = $form;
    }

    public function createForm($type, $data = null, array $options = array())
    {
        return $this->form->create($type, $data, $options);
    }

    public function createFormBuilder($data = null, array $options = array())
    {
        return $this->form->createBuilder('form', $data, $options);
    }
}