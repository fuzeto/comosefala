<?php

namespace ComoSeFala\WebFrameworkBundle\Aware;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Symfony Server Setup: - [ setSession, [@session] ]
 */
trait SessionAware
{
    /**
     * @var Session
     */
    protected $session;

    public function setSession(Session $session)
    {
        $this->session = $session;
    }

}