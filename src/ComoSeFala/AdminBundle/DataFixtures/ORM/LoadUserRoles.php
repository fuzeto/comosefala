<?php

namespace ComoSeFala\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ComoSeFala\DomainBundle\Entity\Role;

class LoadUserRoles extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $role = new Role();
        $role->setname('Administrador');
        $role->setRole('ROLE_ADMIN');
        $role->setCreated(new \DateTime());

        $manager->persist($role);
        $manager->flush();

        $this->addReference('role-admin', $role);
    }
   
     /**
     * The order in wich the fixtures will be loaded
     * @return int
     */
    public function getOrder(){
        return 1; 
    }

}