<?php

namespace ComoSeFala\AdminBundle\DataFixtures\ORM;

use ComoSeFala\DomainBundle\Entity\Counter;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCounter extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $counter = new Counter();
        $counter->setType(Counter::HOME);
        $counter->setCount(0);

        $manager->persist($counter);
        $manager->flush();
    }

     /**
     * The order in wich the fixtures will be loaded
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}
