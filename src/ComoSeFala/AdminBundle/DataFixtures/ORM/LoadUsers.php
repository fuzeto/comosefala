<?php

namespace ComoSeFala\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ComoSeFala\DomainBundle\Entity\User;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUser extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Administrador');
        $user->setEmail('admin@admin.com');
        $user->addRole($this->getReference('role-admin'));
        $user->setPassword($this->encodePassword($user, 'admin'));
        $user->setIsActive(true);
        $user->setCreated(new \Datetime());

        $manager->persist($user);
        $manager->flush();
    }

    private function encodePassword($user, $plainPassword)
    {
        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($user)
        ;

        return $encoder->encodePassword($plainPassword, $user->getSalt());
    }
   
     /**
     * The order in wich the fixtures will be loaded
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}
