<?php

namespace ComoSeFala\AdminBundle\Controller;

use ComoSeFala\DomainBundle\Entity\Counter;
use ComoSeFala\WebFrameworkBundle\Aware\EntityManagerAware;
use ComoSeFala\WebFrameworkBundle\Aware\TwigAware;

class DashboardController
{
    use EntityManagerAware;
    use TwigAware;

    public function indexAction()
    {
        $countVews = $this->em->getRepository('DomainBundle:Counter')
            ->findOneByType(Counter::HOME);

        $countWord = $this->em->getRepository('DomainBundle:Word')
            ->count();;

        return $this->render(
            'AdminBundle:Dashboard:index.html.twig',
            array(
                'countViews' => $countVews->getCount(),
                'countWord'  => $countWord
            )
        );
    }

}