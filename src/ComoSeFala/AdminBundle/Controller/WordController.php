<?php

namespace ComoSeFala\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ComoSeFala\AdminBundle\Form\WordType;
use ComoSeFala\DomainBundle\Entity\Word;
use ComoSeFala\DomainBundle\Service\Word\WordServiceAware;
use ComoSeFala\WebFrameworkBundle\Aware\EntityManagerAware;
use ComoSeFala\WebFrameworkBundle\Aware\FlashMessageAware;
use ComoSeFala\WebFrameworkBundle\Aware\FormAware;
use ComoSeFala\WebFrameworkBundle\Aware\RouterAware;
use ComoSeFala\WebFrameworkBundle\Aware\SecurityAware;
use ComoSeFala\WebFrameworkBundle\Aware\TwigAware;

class WordController
{
    use TwigAware;
    use EntityManagerAware;
    use FormAware;
    use WordServiceAware;
    use SecurityAware;
    use RouterAware;
    use FlashMessageAware;

    public function indexAction()
    {
        $words = $this->em->getRepository('DomainBundle:Word')
            ->all();
        ;

        $count = $this->em->getRepository('DomainBundle:Word')
            ->count();
        ;

        return $this->render(
            'AdminBundle:Word:index.html.twig',
            array(
                'words' => $words,
                'count' => $count
            )
        );
    }

    public function newAction(Request $request)
    {
        $entity = new Word();
        $form   = $this->form->create(new WordType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->wordService->register($entity);
            $this->setFlashMessage('notification', 'Palavra cadastrada com sucesso');
            return $this->redirect($this->generateUrl('word_list'));
        }

        return $this->render(
            'AdminBundle:Word:form.html.twig',
            array(
                'entity' => $entity,
                'form'   => $form->createView(),
            )
        );
    }

    public function editAction(Request $request, Word $word)
    {
        $form   = $this->form->create(new WordType(), $word);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->wordService->update($word);
            $this->setFlashMessage('notification', 'Palavra alterada com sucesso');
            return $this->redirect($this->generateUrl('word_list'));
        }

        return $this->render(
            'AdminBundle:Word:form.html.twig',
            array(
                'entity' => $word,
                'form'   => $form->createView(),
            )
        );
    }

    public function deleteAction(Word $word)
    {
        try {
            $this->wordService->delete($word);
            $this->setFlashMessage('notification', 'Palavra removida com sucesso');
        } catch (\Exception $e) {
            $this->setFlashMessage('error', 'Você não pode remover esta palavra');
        }

        return new JsonResponse(
            array(
                'success' => true
            ),
            200
        );
    }

}