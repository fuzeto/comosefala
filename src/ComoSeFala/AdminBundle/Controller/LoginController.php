<?php
namespace ComoSeFala\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use ComoSeFala\AdminBundle\Form\UserChangePasswordType;
use ComoSeFala\AdminBundle\Form\UserType;
use ComoSeFala\DomainBundle\Entity\User;
use ComoSeFala\DomainBundle\Service\User\UserServiceAware;
use ComoSeFala\WebFrameworkBundle\Aware\FlashMessageAware;
use ComoSeFala\WebFrameworkBundle\Aware\FormAware;
use ComoSeFala\WebFrameworkBundle\Aware\SecurityAware;
use ComoSeFala\WebFrameworkBundle\Aware\TwigAware;

class LoginController
{
    use FlashMessageAware;
    use TwigAware;
    use UserServiceAware;
    use FormAware;
    use SecurityAware;

    public function loginAction()
    {
        $error = $this->session->get(SecurityContext::AUTHENTICATION_ERROR);
        $this->session->remove(SecurityContext::AUTHENTICATION_ERROR);

        return $this->render(
            'AdminBundle:Login:login.html.twig',
            array(
                'last_username' => $this->session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
            )
        );
    }

    public function forgotPasswordAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            try {
                $this->userService->forgotPassword($request->get('email'));
                $this->setFlashMessage('success', 'Nova senha enviada com sucesso');
            } catch (\Exception $e) {
                $this->setFlashMessage('error', 'Dados inválidos');
            }
        }
        return $this->render('AdminBundle:Login:forgot_password.html.twig');
    }

    public function meAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->form->create(new UserType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->userService->update($user);
        }

        return $this->render(
            'AdminBundle:Login:me.html.twig',
            array(
                'tab'  => 'me',
                'form' => $form->createView()
            )
        );
    }

    public function changePasswordAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->form->create(new UserChangePasswordType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->userService->changePassword($user, $form->getData()->getNewPassword());
            $this->setFlashMessage('notification', 'Senha alterada com sucesso');

            /*
             * TODO enviar email.
             */
        }

        return $this->render(
            'AdminBundle:Login:password.html.twig',
            array(
                'tab'  => 'password',
                'form' => $form->createView()
            )
        );
    }
}
