<?php

namespace ComoSeFala\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ComoSeFala\AdminBundle\Form\UserType;
use ComoSeFala\DomainBundle\Entity\User;
use ComoSeFala\DomainBundle\Service\User\UserServiceAware;
use ComoSeFala\WebFrameworkBundle\Aware\EntityManagerAware;
use ComoSeFala\WebFrameworkBundle\Aware\FlashMessageAware;
use ComoSeFala\WebFrameworkBundle\Aware\FormAware;
use ComoSeFala\WebFrameworkBundle\Aware\RouterAware;
use ComoSeFala\WebFrameworkBundle\Aware\SecurityAware;
use ComoSeFala\WebFrameworkBundle\Aware\TwigAware;

class UserController
{
    use TwigAware;
    use EntityManagerAware;
    use FormAware;
    use UserServiceAware;
    use SecurityAware;
    use RouterAware;
    use FlashMessageAware;

    public function indexAction()
    {
        $users = $this->em->getRepository('DomainBundle:User')
            ->allExceptMe($this->getUser());
        ;

        return $this->render(
            'AdminBundle:User:index.html.twig',
            array(
                'users' => $users
            )
        );
    }

    public function newAction(Request $request)
    {
        $entity = new User();
        $form   = $this->form->create(new UserType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->userService->register($entity);
            $this->setFlashMessage('notification', 'Usuário cadastrado com sucesso');
            return $this->redirect($this->generateUrl('user_list'));
        }

        return $this->render(
            'AdminBundle:User:form.html.twig',
            array(
                'entity' => $entity,
                'form'   => $form->createView(),
            )
        );
    }

    public function editAction(Request $request, User $user)
    {
        $form   = $this->form->create(new UserType(), $user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->userService->update($user);
            $this->setFlashMessage('notification', 'Usuário alterado com sucesso');
            return $this->redirect($this->generateUrl('user_list'));
        }

        return $this->render(
            'AdminBundle:User:form.html.twig',
            array(
                'entity' => $user,
                'form'   => $form->createView(),
            )
        );
    }

    public function deleteAction(User $user)
    {
        try {
            $this->userService->delete($user);
            $this->setFlashMessage('notification', 'Usuário removido com sucesso');
        } catch (\Exception $e) {
            $this->setFlashMessage('error', 'Você não pode remover este usuário');
        }

        return new JsonResponse(
            array(
                'success' => true
            ),
            200
        );
    }

}