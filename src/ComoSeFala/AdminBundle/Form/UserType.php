<?php

namespace ComoSeFala\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                array(
                    'label' => 'Nome',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )

            ->add(
                'email',
                'text',
                array(
                    'label' => 'E-mail',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )

            ->add(
                'roles',
                'entity',
                array(
                    'class'       => 'DomainBundle:Role',
                    'label'       => 'Perfil',
                    'empty_value' => 'Selecione',
                    'multiple'    => true,
                    'attr' => array(
                        'style' => 'width: 100%',
                        'data-placeholder' => "Selecione",
                        'autocomplete' => 'off',
                        'class' => 'selectboxit'
                    )
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ComoSeFala\DomainBundle\Entity\User',
                'profile_page' => false
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user';
    }
}
