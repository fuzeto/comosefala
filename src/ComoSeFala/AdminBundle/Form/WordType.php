<?php

namespace ComoSeFala\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'word',
                'text',
                array(
                    'label' => 'Palavra',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            )

            ->add(
                'pronounce',
                'text',
                array(
                    'label' => 'Como se fala?',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ComoSeFala\DomainBundle\Entity\Word')
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'word';
    }
}
