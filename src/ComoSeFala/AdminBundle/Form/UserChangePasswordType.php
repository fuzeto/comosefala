<?php

namespace ComoSeFala\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserChangePasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('currentPassword', 'password', array(
                'label' => 'Senha Atual',
                'attr' => array(
                    'maxlength' => '8',
                    'autocomplete' => 'off',
                    'class' => 'form-control'
                ),
            ))

            ->add('newPassword', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'As senhas devem ser iguais.',
                'options' => array(
                    'attr' => array(
                        'maxlength' => '8',
                        'class' => 'form-control'
                    )
                ),
                'required' => true,
                'first_options'  => array('label' => 'Nova Senha'),
                'second_options' => array('label' => 'Repita a Senha'),
            ))
        ;

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => "ComoSeFala\\DomainBundle\\Entity\\User",
            'admin'             => true,
            'validation_groups' => array('Default', 'password')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'change_password';
    }
}
