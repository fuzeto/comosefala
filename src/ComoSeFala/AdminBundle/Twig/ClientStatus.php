<?php
namespace ComoSeFala\AdminBundle\Twig;

use ComoSeFala\DomainBundle\Entity\Client;

class ClientStatus extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('client_status', array($this, 'getClientStatus')),
        );
    }

    public function getClientStatus($status)
    {
        $client = new Client();
        $client->setStatus($status);

        return $client->getStatusAsString();
    }

    public function getName()
    {
        return 'client_status';
    }
}
