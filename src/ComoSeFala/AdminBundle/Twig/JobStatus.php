<?php
namespace ComoSeFala\AdminBundle\Twig;

use ComoSeFala\DomainBundle\Entity\Job;

class JobStatus extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('job_status', array($this, 'getJobStatus')),
        );
    }

    public function getJobStatus($status)
    {
        $job = new Job();
        $job->setStatus($status);

        return $job->getStatusAsString();
    }

    public function getName()
    {
        return 'job_status';
    }
}
